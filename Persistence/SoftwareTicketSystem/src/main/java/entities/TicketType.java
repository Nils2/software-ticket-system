package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TicketType {
	
	@Id @GeneratedValue
	private Long id;
	private String type;
}
